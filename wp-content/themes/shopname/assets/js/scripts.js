(function($) {
    $(document).ready(function() {


        function init_page_dev(){



				/* build and append preformated code examples */
                $("div#content").find("p").last().after(buildCodeBlocks);

				/* toggle/collapse preformated code blocks */
                $("pre").bind("click", function(){
                    this.className = this.className.indexOf("collapse") === -1 ?
                        ( "collapse " + this.className ) : this.className.replace("collapse ", "");
                });

				/* apply code highlight */
                $('pre code').each( function(i, e) {
                    hljs.highlightBlock(e, '    ');
                });



            function buildCodeBlocks() {
                return "<div class='codeBlocks clearfix'>" +
                    "<pre class='html'><code>" + getHtml() + "</code></pre>" +
                    "<pre class='javascript'><code>" + cleanJson( $("head script").last().text() ) + "</code></pre>" +
                    "<pre class='css'><code>" + cleanCSS( $("head style").text() ) + "</code></pre>" +
                    "</div>";
            }


            function getHtml() {
                var clone, ul, li, code;

                clone = $("<div />").append($("div#content").contents().not("h2, p").clone());
                ul = clone.find("div");
                li = ul.find("div");

                li.slice(3, li.length).remove();
                ul.append("...");

                code = clone.html();

                return cleanHTML(code).replace(/</gi, "&lt;").replace(/>/gi, "&gt;");;
                //code = $.tabifier(code, "HTML").replace(/</gi, "&lt;").replace(/>/gi, "&gt;");

            }
		}
        

	    $("#filter-apart").submit(function(e){

            var data_section = [];

            var data_queue = [];

            var data_room = [];

            $('.queue:checked').each( function() {
                data_queue.push($(this).val());
            } );

            $('.room:checked').each( function() {
                data_room.push($(this).val());
            } );

            var data_readiness = [];

            $('.readiness:checked').each( function() {
                data_readiness.push($(this).val());
            } );


            $(".section:checked").each(function() {
                data_section.push($(this).val());
            });

            var data_max = $("#amountMax").val();

            var data_installments = $(".installments:checked").val();

            var data_min = $("#amountMin").val();


			$('.respons-filter').html('');
			e.preventDefault();
	
			var data_storey = $('.storey' ).val();
			var data_id = $(this).attr("data-id");
			$.ajax({
				type: "POST",
				url: shopname.url,
				data: {
					data_id: data_id,
					data_queue: data_queue,
                    data_min: data_min,
                    data_max: data_max,
                    data_installments: data_installments,
                    data_room: data_room,
                    data_readiness: data_readiness,
                    data_section: data_section,
					security: shopname.nonce,
					action: 'apart_filter'
				},
				beforeSend : function(){
					// $('#image-res').show();

				},
				success: function(res){
					// $('#image-res').hide();
					$(".respons-filter").html(res);
					$(".pagination-apart").hide();
					/* initiate plugin */
                    if ( $('.card.flats__item').length > 9 ) {
                        $("ul.holder").jPages({
                            containerID : "itemContainer"
                        });
                    }
                    $(".removeclass").removeClass('flats__items_wrapper');
                    $('.respons-filter').removeClass('row');
                    $("#itemContainer").addClass('flats__items_wrapper row');
				},
				error: function(){
					console.log("Error !!!")
				}
			});
		});

	    // Advantages pagination

		/*$('.advantages__btn').on('click', function() {

            var $advantagesBtn = $('#advantagesPagination').find('.advantages__btn');
            $advantagesBtn.removeClass('active');
            console.log($advantagesBtn);
            $(this).addClass('active');

		});*/

        $(document).on( "click", '.advantages__btn', function () {
            var $advantagesBtn = $('#advantagesPagination').find('.advantages__btn');
            $advantagesBtn.removeClass('active');
            $(this).addClass('active');
        });

        // Slider Dots

        var adv_btn = $('.advantages__btn');

        adv_btn.click(function() {
            var slide_id = $(this).data('id');


            $('#advantages-custom-dots .owl-dot').removeClass('active');
            $('#advantages-custom-dots .id-' + slide_id).addClass('active');
        });

        var adv_dot = $('#advantages-custom-dots .owl-dot');

        adv_dot.click(function() {
            var dote_id = $(this).data('id');

            $('#advantagesPagination .advantages__btn').removeClass('active');
            $('#advantagesPagination .id-' + dote_id).addClass('active');
        });

        /**
         * Contact Form 7 Events
         */

        document.addEventListener( 'wpcf7mailsent', function( event ) {
            document.getElementById('contactform').style.display = 'none';
            setTimeout(function () {
            	$('.wpcf7-mail-sent-ok').hide();
				$('#contactform').slideDown();
            }, 5000);
        }, false );






        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });



        $('.block2-btn-addcart').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function(){
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function(){
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });


        $('.parallax100').parallax100();




        $(window).scroll(function(){
            var $progressCircle = $('.header__brand');
            var windowScroll = $(window).scrollTop();

            if ( $progressCircle.length > 0 ) {
                if ( windowScroll > 30 ) {
                    $progressCircle.addClass('small-logo')
                } else {
                    $progressCircle.removeClass('small-logo')
                }
            }
        });
        
    });
})(jQuery);
