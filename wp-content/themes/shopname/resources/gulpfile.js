var gulp = require('gulp');
var sass = require('gulp-sass');

var concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify');

gulp.task('scss', function() {
    gulp.src('sass/global.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('../assets/css'));
});

gulp.task('scripts', function() {
    gulp.src('js/**/*.js')
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('../assets/js'))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/js'));
});

var libs = [
    'vendor/animsition/js/animsition.min.js',
    'vendor/bootstrap/js/popper.js',
    'vendor/bootstrap/js/bootstrap.min.js',
    'vendor/select2/select2.min.js',
    'vendor/slick/slick.min.js',
    'vendor/countdowntime/countdowntime.js',
    'vendor/lightbox2/js/lightbox.min.js',
    'vendor/sweetalert/sweetalert.min.js',
    'vendor/parallax100/parallax100.js',
    'js-libs/main.js',
    'js-libs/slick-custom.js',
    'node_modules/tether/dist/js/tether.min.js',
    'node_modules/owl.carousel/dist/owl.carousel.min.js',
    'node_modules/threesixty-slider/dist/threesixty.min.js'

];

gulp.task('libs-scripts', function() {
    gulp.src(libs)
        .pipe(concat('libs-scripts.js'))
        .pipe(gulp.dest('../assets/js'))
        .pipe(rename('libs-scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/js'));
});

var openlayers = [
    'node_modules/openlayers/dist/ol.js'
];

gulp.task('openlayers', function() {
    gulp.src(openlayers)
        .pipe(rename('openlayers.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/js'));
});

gulp.task('default', function() {
    gulp.run("scss");
    gulp.run("scripts");
    gulp.run("libs-scripts");
    gulp.run("openlayers");

    gulp.watch('./sass/**/*.scss', function(event) {
        gulp.run('scss');
    });

    gulp.watch('js/**/*.js', function(event) {
        gulp.run('scripts');
    });
});
