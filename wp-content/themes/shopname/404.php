<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

get_header(); $current_lang = get_locale(); ?>

    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <section class="error-404 not-found section__error bg-grey" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/news/article2.png);">
                    <div class="container text-center">
                        <header class="page-header">
                            <h1 class="page-title"><?php _e( 'Страница не найдена', THEME_OPT ); ?></h1>
                        </header><!-- .page-header -->
                        <div class="page-content">
                            <a role="button" href="<?php echo ( $current_lang == 'ua' ) ? get_page_link( APPART_PG_ID_UK ) : get_page_link(APPART_PG_ID); ?>" class="btn btn-secondary btn-lg"><?php _e( 'Квартиры', THEME_OPT ); ?></a>
                        </div><!-- .page-content -->
                    </div>
                </section><!-- .error-404 -->
            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- .wrap -->

<?php get_template_part('templates/parts/page/content', 'footer'); ?>

<?php get_footer(); ?>
