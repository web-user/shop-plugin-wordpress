<?php
/**
 * The template for displaying all single Brand posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <?php if (get_field('add_fields') && in_array('developer', get_field('add_fields'))) : // Developer ?>
        <a class="link__return" href="<?php echo get_home_url() ?>/#anchorBlockAbout">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <?php _e('Назад', THEME_OPT); ?>
        </a>
    <?php elseif (get_field('add_fields') && in_array('complex', get_field('add_fields'))) : // Complex ?>
        <a class="link__return" href="<?php echo get_home_url() ?>/#anchorBlockFeature">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <?php _e('Назад', THEME_OPT); ?>
        </a>
    <?php elseif ( wp_get_referer() ) : ?>
        <a class="link__return" href="<?php echo wp_get_referer() ?>">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <?php _e('Назад', THEME_OPT); ?>
        </a>
    <?php else : ?>
        <a class="link__return" href="<?php echo get_home_url() ?>">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <?php _e('Назад', THEME_OPT); ?>
        </a>
    <?php endif; ?>
    <div id="preloader"></div>
    <div id="scrollBlock3" class="brand">

        <!-- Navigation -->
        <nav id="scrollBlockMenu3" class="navbar fixed__navigation fixed__navigation_brand">
            <ul class="nav">
                <li class="nav-item" data-menuanchor="mainInfo">
                    <a class="nav-link fixed__navigation_link fixed__navigation_link_white" href="#mainInfo">
                        <span class="fixed__navigation_line"><span></span></span>
                        <?php the_field('brand_first_title'); ?>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="mainPhotos">
                    <a class="nav-link fixed__navigation_link fixed__navigation_link_white" href="#mainPhotos">
                        <span class="fixed__navigation_line"><span></span></span>
                        <?php the_field('brand_third_title'); ?>
                    </a>
                </li>
                <li class="nav-item" data-menuanchor="mainAbout">
                    <a class="nav-link fixed__navigation_link fixed__navigation_link_black" href="#mainAbout">
                        <span class="fixed__navigation_line"><span></span></span>
                        <?php the_field('brand_second_title'); ?>
                    </a>
                </li>
            </ul>
        </nav>

        <h1 class="sr-only"><?php _e('Инфрастуктура', THEME_OPT); ?></h1>

            <?php
            /* Start the Loop */
            while ( have_posts() ) : the_post();

                get_template_part( 'templates/parts/brand/content' );

            endwhile; // End of the loop.
            ?>

        <?php get_template_part('templates/parts/page/content', 'footer'); ?>
    </div>

<?php get_footer(); ?>