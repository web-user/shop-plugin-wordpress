<?php
/**
 * The main template file
 *
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <div class="wrap">
        <header class="page-header">
            <h1 class="page-title"><?php the_title(); ?></h1>
        </header>

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php
                if ( have_posts() ) :

                    /* Start the Loop */
                    while ( have_posts() ) : the_post();

                        get_template_part( 'templates/parts/post/content' );

                    endwhile;

                    // pagination

                else :

                    get_template_part( 'templates/parts/post/content', 'none' );

                endif;
                ?>

            </main><!-- #main -->
        </div><!-- #primary -->

    </div><!-- .wrap -->

<?php get_template_part('templates/parts/page/content', 'footer'); ?>

<?php get_footer(); ?>