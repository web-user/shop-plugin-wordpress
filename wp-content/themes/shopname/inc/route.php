<?php

/**
 * Custom WP Routing rules
 *
 */
function shopname_route() {
    global $wp_rewrite;

//    /* News */
//    add_rewrite_rule(
//        '^news/([^/]*)/?$',
//        'index.php?page_id=23&current_cat=$matches[1]',
//        'top'
//    );
//    add_rewrite_rule(
//        '^news/([^/]*)/page/([^/]*)?$',
//        'index.php?page_id=23&current_cat=$matches[1]&paged=$matches[2]',
//        'top'
//    );
//
//    /* News UA*/
//    add_rewrite_rule(
//        '^ua/news/([^/]*)/?$',
//        'index.php?page_id=544&current_cat=$matches[1]',
//        'top'
//    );
//    add_rewrite_rule(
//        '^ua/news/([^/]*)/page/([^/]*)?$',
//        'index.php?page_id=544&current_cat=$matches[1]&paged=$matches[2]',
//        'top'
//    );

    /* Gallery */
    add_rewrite_rule(
        '^gallery/([^/]*)/?$',
        'index.php?page_id=19&current_gal=$matches[1]',
        'top'
    );
    add_rewrite_rule(
        '^ua/gallery/([^/]*)/?$',
        'index.php?page_id=498&current_gal=$matches[1]',
        'top'
    );

    /* Developer */
    add_rewrite_rule(
        '^zastroyshchik',
        'index.php?page_id=299',
        'top'
    );
    add_rewrite_rule(
        '^ua/zastroyshchik',
        'index.php?page_id=299',
        'top'
    );

    add_filter( 'query_vars', function( $vars ){
        $vars[] = 'current_cat';
        $vars[] = 'current_gal';

        return $vars;
    } );
    $wp_rewrite->flush_rules(false);
}
add_action('init', 'shopname_route');

/**
 * Redirects
 *
 */

function shopname_redirect() {

    //apartment_redirect
    $queried_post_type = get_query_var('post_type');
    if ( is_single() && 'posst_apartment' ==  $queried_post_type ) {
        $appart_url = ( get_locale() == 'ua' ) ? get_page_link( APPART_PG_ID_UK ) : get_page_link(APPART_PG_ID);
        wp_redirect( $appart_url, 301 );
        exit;
    }

    if (preg_match('#^/novostroyki-kieva/?$#i', $_SERVER['REQUEST_URI'])) {
        wp_redirect( home_url(), 301 );
        exit;
    }
    if (preg_match('#^/ua/novostroyki-kieva/?$#i', $_SERVER['REQUEST_URI'])) {
        wp_redirect( home_url('/ua'), 301 );
        exit;
    }

    if (preg_match('#^/about/?$#i', $_SERVER['REQUEST_URI'])) {
        wp_redirect( home_url('/#anchorBlockAbout'), 301 );
        exit;
    }
    if (preg_match('#^/ua/about/?$#i', $_SERVER['REQUEST_URI'])) {
        wp_redirect( home_url('/ua/#anchorBlockAbout'), 301 );
        exit;
    }

    if (preg_match('#^/kupit-kvartiru-pod-kievom-ot-zastroyshika/?$#i', $_SERVER['REQUEST_URI'])
        || preg_match('#^/kvartiry-kiev/1-komnatnye/?$#i', $_SERVER['REQUEST_URI']) ) {
        wp_redirect( home_url('/kvartiry-kiev'), 301 );
        exit;
    }
    if (preg_match('#^/ua/kupit-kvartiru-pod-kievom-ot-zastroyshika/?$#i', $_SERVER['REQUEST_URI'])
        || preg_match('#^/ua/kvartiry-kiev/1-komnatnye/?$#i', $_SERVER['REQUEST_URI']) ) {
        wp_redirect( home_url('/ua/kvartiry-kiev'), 301 );
        exit;
    }

    if (preg_match('#^/kupit-kvartiru-v-krykovshine/?$#i', $_SERVER['REQUEST_URI'])
        || preg_match('#^/kupit-kvartiru-v-vishnevom/?$#i', $_SERVER['REQUEST_URI']) ) {
        wp_redirect( home_url('/infrastructure/#infrastructureMap'), 301 );
        exit;
    }
    if (preg_match('#^/ua/kupit-kvartiru-v-krykovshine/?$#i', $_SERVER['REQUEST_URI'])
        || preg_match('#^/ua/kupit-kvartiru-v-vishnevom/?$#i', $_SERVER['REQUEST_URI']) ) {
        wp_redirect( home_url('/infrastructure/#infrastructureMap'), 301 );
        exit;
    }

}

add_action( 'template_redirect', 'shopname_redirect' );