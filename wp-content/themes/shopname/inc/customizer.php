<?php
/**
 * shopname: Customizer
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function shopname_customize_register( $wp_customize ) {

    /**
     * Header
     */
    $wp_customize->add_section(
        'header_section',
        array(
            'title' => __('Хедер', 'em-customizer'),
            'description' => 'This is a settings section.',
            'priority' => 30,
        )
    );

    /**
     * Image: Logo
     */
    $wp_customize->add_setting('header_logo', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'              => 'theme_mod',

    ));

    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'upload_header_logo', array(
        'label'    => __('Лого', 'em-customizer'),
        'section'  => 'header_section',
        'settings' => 'header_logo',
    )));


    /**
     * Footer
     */

    $wp_customize->add_section(
        'footer_section',
        array(
            'title' => __('Футер', 'em-customizer'),
            'description' => 'This is a settings section.',
            'priority' => 35,
        )
    );

    /**
     * Image: First Logo
     */
    $wp_customize->add_setting('first_footer_logo', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'              => 'theme_mod',

    ));

    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'upload_first_logo', array(
        'label'    => __('Первое Лого', 'em-customizer'),
        'section'  => 'footer_section',
        'settings' => 'first_footer_logo',
    )));

    /**
     * Image: Second Logo
     */
    $wp_customize->add_setting('second_footer_logo', array(
        'default'           => '',
        'capability'        => 'edit_theme_options',
        'type'              => 'theme_mod',

    ));

    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'upload_second_logo', array(
        'label'    => __('Второе Лого', 'em-customizer'),
        'section'  => 'footer_section',
        'settings' => 'second_footer_logo',
    )));

    /**
     * Phone
     */
    $wp_customize->add_setting('footer_phone', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',

    ));

    $wp_customize->add_control('shopname_footer_phone', array(
        'label'      => __('Телефон', 'em-customizer'),
        'section'    => 'footer_section',
        'settings'   => 'footer_phone',
    ));

    /**
     * Email
     */
    $wp_customize->add_setting('footer_email', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',

    ));

    $wp_customize->add_control('shopname_footer_email', array(
        'label'      => __('Email', 'em-customizer'),
        'section'    => 'footer_section',
        'settings'   => 'footer_email',
    ));

    /**
     * Social
     */
//    $wp_customize->add_setting('footer_soc_vk', array(
//        'default'        => '',
//        'capability'     => 'edit_theme_options',
//        'type'           => 'theme_mod',
//
//    ));
//
//    $wp_customize->add_control('shopname_footer_vk', array(
//        'label'      => __('VK', 'em-customizer'),
//        'section'    => 'footer_section',
//        'settings'   => 'footer_soc_vk',
//    ));

    $wp_customize->add_setting('footer_soc_fb', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',

    ));
    $wp_customize->add_control('shopname_footer_fb', array(
        'label'      => __('Facebook', 'em-customizer'),
        'section'    => 'footer_section',
        'settings'   => 'footer_soc_fb',
    ));

    $wp_customize->add_setting('footer_soc_inst', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',

    ));
    $wp_customize->add_control('shopname_footer_inst', array(
        'label'      => __('Instagram', 'em-customizer'),
        'section'    => 'footer_section',
        'settings'   => 'footer_soc_inst',
    ));
}
add_action( 'customize_register', 'shopname_customize_register' );