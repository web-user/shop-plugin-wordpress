<?php
/**
 * Restrict mime types
 *
 */

if ( ! function_exists( 'em_restrict_mime_types' ) ) {

    add_filter( 'upload_mimes', 'em_restrict_mime_types' );
    /**
     * Retrun allowed mime types
     *
     * @see     function get_allowed_mime_types in wp-includes/functions.php
     * @param   array Array of mime types
     * @return  array Array of mime types keyed by the file extension regex corresponding to those types.
     */
    function em_restrict_mime_types( $mime_types ) {

        $mime_types = array(
            'jpg|jpeg|jpe' => 'image/jpeg',
            'gif'          => 'image/gif',
            'png'          => 'image/png',
            'svg'          => 'image/svg+xml',
            'webm'         => 'video/webm',
            'flv'          => 'video/x-flv',
            'mp4|m4v'      => 'video/mp4',
        );

        return $mime_types;
    }
}

// If the function exists this file is called as post-upload-ui.
// We don't do anything then.
if ( ! function_exists( 'fb_restrict_mime_types_hint' ) ) {
    // add to wp
    add_action( 'post-upload-ui', 'em_restrict_mime_types_hint' );
    /**
     * Get an Hint about the allowed mime types
     *
     * @return  void
     */
    function em_restrict_mime_types_hint() {

        echo '<br>';
        _e( 'Поддерживаемые форматы изображения: jpg, gif, png, svg', THEME_OPT );
        echo '<br>';
        _e( 'Поддерживаемые форматы видео: WEBM, FLV, MP4', THEME_OPT );
    }
}


/**
 * Customize uploading files
 *
 *
 * @param $file
 * @return mixed
 */
function custom_upload_filter( $file ) {
    $img_types = array(
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif'          => 'image/gif',
        'png'          => 'image/png',
        'svg'          => 'image/svg+xml',
    );
    $video_types = array(
        'webm'         => 'video/webm',
        'flv'          => 'video/x-flv',
        'mp4|m4v'      => 'video/mp4',
    );

    if (in_array($file['type'], $img_types)) {
        if ($file['size'] > 3145728) { // max 3 MB
            $error = array(
                'error' => __('Максимальный размер картинки 3МБ', THEME_OPT),
            );
            return $error;
        }
    }
    if (in_array($file['type'], $video_types)) {
        if ($file['size'] > 20971520) { // max 15 MB
            $error = array(
                'error' => __('Максимальный размер video 20МБ', THEME_OPT),
            );
            return $error;
        }
    }

    return $file;
}
add_filter('wp_handle_upload_prefilter', 'custom_upload_filter' );