<?php

/**
 * The template for displaying post category
 *
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */
$term_obj = get_queried_object();
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

$current_lang = get_locale();
$current_cat = get_query_var('category_name');

get_header(); ?>
<section class="news">
    <h1 class="sr-only"><?php _e( 'Новости', THEME_OPT ); ?></h1>
    <div class="container">
        <div class="row">

            <!-- Left Sidebar -->
            <div class="col-lg-3">
                <div class="buttons__navigation">
                    <?php
                    $args = array(
                        'type'         => 'post',
                        'taxonomy'     => 'category',
                    );

                    $categories = get_categories( $args );
                    if ($categories) :


                        foreach ($categories as $cat) :
                            $active = ($cat->slug == $current_cat) ? 'active' : ''; ?>

                            <a href="<?php echo ( $current_lang == 'ua' ) ? home_url('ua/news') .'/'.$cat->slug : home_url('news') .'/'.$cat->slug; ?>" class="buttons__navigation_item <?php echo $active; ?>"><?php echo $cat->name; ?></a>
                        <?php endforeach; ?>

                        <button type="button" class="buttons__navigation_item-toggle dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu buttons__navigation_dropdown">
                            <?php foreach ($categories as $cat) :
                                $active_dd = ($cat->slug == $current_cat) ? 'active' : ''; ?>

                                <a href="<?php echo ( $current_lang == 'ua' ) ? home_url('ua/news') .'/'.$cat->slug : home_url('news') .'/'.$cat->slug; ?>" class="buttons__navigation_item <?php echo $active_dd; ?>"><?php echo $cat->name; ?></a>

                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php get_template_part( 'templates/parts/post/list' ); ?>

        </div><!-- .row -->
    </div>
</section>

<?php get_template_part('templates/parts/page/content', 'footer'); ?>

<?php get_footer(); ?>