<?php
/**
 * shopname functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 */

define('THEME_OPT', 'shopname');
define('APPART_PG_ID', 17);
define('APPART_PG_ID_UK', 464);
define('INFR_PG_ID', 21);
define('INFR_PG_ID_UK', 551);
define('NEWS_PG_ID', 23);
define('NEWS_PG_ID_UK', 544);
define('CURRENT_LANG', get_locale());

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function shopname_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
     * If you're building a theme based on Twenty Seventeen, use a find and replace
     * to change THEME_OPT to the name of your theme in all the template files.
     */
    load_theme_textdomain(THEME_OPT, get_stylesheet_directory().'/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
//    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'shopname-featured-image', 2000, 1200, true );

    add_image_size( 'shopname-thumbnail-avatar', 100, 100, true );

    // Set the default content width.
    $GLOBALS['content_width'] = 525;

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'top'       => __( 'Top Menu', THEME_OPT ),
        'bottom'    => __( 'Bottom Menu', THEME_OPT ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    /*
     * Enable support for Post Formats.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    ) );

    // Add theme support for Custom Logo.
    add_theme_support( 'custom-logo', array(
        'width'       => 250,
        'height'      => 250,
        'flex-width'  => true,
    ) );

    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'shopname_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function shopname_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'News Sidebar', THEME_OPT ),
        'id'            => 'news-sidebar',
        'description'   => __( 'Add widgets here to appear in your sidebar.', THEME_OPT ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Sidebar menu', THEME_OPT ),
        'id'            => 'menu_sidebar',
        'description'   => __( 'Add widgets here to appear in your sidebar.', THEME_OPT ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );
}
add_action( 'widgets_init', 'shopname_widgets_init' );


/**
 * Enqueue styles.
 */
function shopname_styles() {

    // Theme stylesheet.
    wp_enqueue_style( 'shopname-style', get_stylesheet_uri() );
    wp_enqueue_style( 'shopname-css', get_stylesheet_directory_uri() . '/assets/css/global.css' );
    wp_enqueue_style( 'shopname-vendor-css', get_stylesheet_directory_uri() . '/assets/vendor/global_vendor.css' );
}
add_action( 'wp_enqueue_scripts', 'shopname_styles' );

/**
 * Enqueue scripts.
 */
function shopname_scripts() {



    wp_enqueue_script( 'libs-js', get_stylesheet_directory_uri() . '/assets/js/libs-scripts.min.js', array('jquery'), false, true );




    wp_enqueue_script( 'shopname-js', get_stylesheet_directory_uri() . '/assets/js/scripts.min.js', array('jquery'), false, true );

}
add_action( 'wp_enqueue_scripts', 'shopname_scripts' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Читать далее' link.
 *
 * @since shopname 1.0
 *
 * @return string 'Читать далее' link prepended with an ellipsis.
 */
function shopname_excerpt_more( $link ) {
    return '...';
}
add_filter( 'excerpt_more', 'shopname_excerpt_more' );

/**
 * Change Excerpt length
 *
 * @param $length
 * @return int
 */
function shopname_excerpt_length( $length ) {
    return 50;
}
add_filter( 'excerpt_length', 'shopname_excerpt_length', 999 );

/**
 * Get excerpt by post id
 *
 * @param $post_id
 * @return string
 */
function shopname_get_the_excerpt($post_id) {
    global $post;

    $save_post = $post;
    $post = get_post($post_id);
    $output = get_the_excerpt();
    $post = $save_post;

    return $output;
}


/**
 * Pagination
 *
 * @since shopname 1.0
 */


function get_pagenavi_array($wp_query) {
    $big = 999999999;

    $args = array(
        'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
        'type'      => 'array',
        'mid_size'  => 1,
        'end_size'  => 1,
        'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i><span class="sr-only">Previous</span>',
        'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i><span class="sr-only">Next</span>',
        'current'   => max( 1, get_query_var('paged') ),
        'total'     => $wp_query->max_num_pages,
    );

    $result = paginate_links( $args );

    $result = str_replace( '/page/1/', '', $result );

    return $result;
}





function ajax_apart_filter_get() {

    if ( isset( $_POST['action'] ) ){




        if (  isset( $_POST['data_room'] ) || isset( $_POST['data_readiness'] )  || isset( $_POST['data_installments'] ) || $_POST['data_min'] > 36 || $_POST['data_max'] < 81 ) {


            // Build the meta query based on the selected options

            $meta_query = array( 'relation' => 'AND', 'room_clause' => array( 'key' => 'room', ), 'total_area_clause' => array( 'key' => 'total_area', ) );
            if ( isset( $_POST['data_section'] ) ){
                // Get the selected options
//                $my_acf_checkbox_field_arr = $_POST['data_section'];
//                if ( is_array( $my_acf_checkbox_field_arr ) ){
//                    foreach( $my_acf_checkbox_field_arr as $item ){
//                        $meta_query[] = array(
//                            'key'     => 'section',
//                            'value'   => $item,
//                            'compare' => 'LIKE',
//                        );
//                    }
//                }

            }

            if ( isset( $_POST['data_room'] ) ){
                $meta_query[] = array(
                    'key'     => 'room',
                    'value'   => $_POST['data_room']
                );
            }


//            if ( isset( $_POST['data_queue'] ) ){
//                $meta_query[] = array(
//                    'key'     => 'queue',
//                    'value'   => $_POST['data_queue']
//                );
//            }

            if( isset( $_POST['data_readiness'] ) ){
                $readiness_storey = $_POST['data_readiness'];

                $meta_query[] = array(
                    'key'     => 'readiness',
                    'value'   => $readiness_storey
                );
            }



            if ( isset( $_POST['data_installments'] ) ){
                $meta_query[] = array(
                    'key'     => 'installments',
                    'value'   => $_POST['data_installments'],
                    'compare' => 'LIKE'
                );
            }


            $meta_query[] = array(
                'key' => 'total_area',
                'value' => array( $_POST['data_min'],$_POST['data_max'] ),
                'type' => 'NUMERIC',
                'compare' => 'BETWEEN',
            );
//
//            $meta_query_root = array( 'room_clause' =>  array( 'key' => 'room' ) );
//
//            $meta_query_total_area = array( 'total_area_clause' =>  array( 'key' => 'total_area' ) );



            if( get_locale() == 'ua' ){
                // args
                $args = array(
                    'posts_per_page' => -1,
                    'numberposts'	=> -1,
                    'lang'           => 'ua',    // use language slug in the query
                    'post_type'		=> 'posst_apartment',
                    'post_status'=>'publish',
                    'meta_query' => $meta_query,
                    'orderby' => array(
                        'room_clause' => 'ASC',
                        'total_area_clause' => 'ASC',
                    ),
                );

            } else {
                // args
                $args = array(
                    'posts_per_page' => -1,
                    'numberposts'	=> -1,
                    'post_type'		=> 'posst_apartment',
                    'post_status'=>'publish',
                    'meta_query' => $meta_query,
                    'orderby' => array(
                        'room_clause' => 'ASC',
                        'total_area_clause' => 'ASC',
                    ),
                );

            }



        } else {

            // locale
            if( get_locale() == 'ua' ){
                // args
                $args = array(
                    'posts_per_page' => -1,
                    'numberposts'	=> -1,
                    'lang'           => 'ua',    // use language slug in the query
                    'post_type'		=> 'posst_apartment',
                    'post_status'=>'publish',
                    'meta_query' => array(
                        'relation' => 'AND',
                        'room_clause' => array(
                            'key' => 'room',
                        ),
                        'total_area_clause' => array(
                            'key' => 'total_area',
                        ),
                    ),
                    'orderby' => array(
                        'room_clause' => 'ASC',
                        'total_area_clause' => 'ASC',
                    ),
                );
            } else {
                // args
                $args = array(
                    'posts_per_page' => -1,
                    'numberposts'	=> -1,
                    'post_type'		=> 'posst_apartment',
                    'post_status'=>'publish',
                    'meta_query' => array(
                        'relation' => 'AND',
                        'room_clause' => array(
                            'key' => 'room',
                        ),
                        'total_area_clause' => array(
                            'key' => 'total_area',
                        ),
                    ),
                    'orderby' => array(
                        'room_clause' => 'ASC',
                        'total_area_clause' => 'ASC',
                    ),
                );
            }

        }









        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            echo '<div id="content" class="defaults">';
                echo '<div id="itemContainer">';
                    while ( $query->have_posts() ) : $query->the_post();
                        get_template_part( 'templates/parts/apartment/content' );
                    endwhile;
                echo '</div>';
                echo '<ul class="holder pagination justify-content-center news__items_pagination"></ul>';
            echo '</div>';
            wp_reset_postdata();
        else:
            echo "<div class='col-12 new-test'><h3>" . __( "Не найдено ни одной квартиры по заданным параметрам", THEME_OPT ) . "</h3></div>";
        endif;

    }

    wp_die(); // this is required to terminate immediately and return a proper response


}

add_action( 'wp_ajax_apart_filter', 'ajax_apart_filter_get' );
add_action( 'wp_ajax_nopriv_apart_filter', 'ajax_apart_filter_get' );



/**
 * Gallery Photo
 *
 * Get Full Image by thumbnail id by click
 * @use ajax
 *
 * @return string
 */

function gallery_photo() {
    extract($_REQUEST);

    if (isset($thumb_id)) {
        $url = wp_get_attachment_image_src($thumb_id, 'large');

        echo $url[0];
    }

    wp_die();
}

add_action( 'wp_ajax_gallery_photo', 'gallery_photo' );
add_action( 'wp_ajax_nopriv_gallery_photo', 'gallery_photo' );


/**
 * Retrieves the attachment ID from the file URL
 *
 * @param $image_url
 * @return mixed
 */
function get_image_id($image_url) {
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
    return $attachment[0];
}

/**
 * Gallery Photo
 *
 * Get Full Image by thumbnail id by click
 * @use ajax
 *
 * @return string
 */

function ajax_gallery_360_get() {

    if ( isset( $_POST['data_id'] ) ) {



        $query_apart = new WP_Query( array( 'p' => $_POST['data_id'], 'post_type' => 'posst_apartment',  ) );

         while ( $query_apart->have_posts() ) : $query_apart->the_post();
             get_template_part( 'templates/parts/gall-360/content' );
         endwhile;

    }

    wp_die();
}




add_action( 'wp_ajax_gallery_360', 'ajax_gallery_360_get' );
add_action( 'wp_ajax_nopriv_gallery_360', 'ajax_gallery_360_get' );




/**
 * Get html article by id
 *
 * @param $post_id
 * @return bool|string
 */

function get_article($post_id) {
    if (! $post_id)
        return false;

    $is_current = ($post_id == $post->ID) ? 'current' : '';

    $html = '
        <article id="post-'.$post_id.'" '. implode(' ', get_post_class($is_current, $post_id)) .'>
            <div class="card news__item">';
                if (has_post_thumbnail($post_id)) :
                    $html .= '
                    <div class="news__item_img" style="background-image: url('. get_the_post_thumbnail_url($post_id, "full").')" alt="Card image cap">
                        <div class="news__item_date">
                            <div class="day">'. get_the_date("d", $post_id). '</div>
                            <div class="month">'. get_the_date("M", $post_id). '</div>
                            <div class="year">'. get_the_date("Y", $post_id). '</div>
                        </div>
                    </div>';
                endif;
            $html .= '
                <div class="card-block news__item_header">
                    <h2 class="card-title news__item_title">';
                        $strings_actual_encoding = 'utf8';
                        $s_noentities = html_entity_decode(get_the_title(), ENT_QUOTES, $strings_actual_encoding);
                        $html .= mb_substr($s_noentities, 0, 36, $strings_actual_encoding);
                        $html .= (mb_substr(get_the_title($post_id), 37)) ? "..." : "";
            $html .= '</h2>
                    <span class="news__item_tag">';
                            $terms = wp_get_post_terms(get_the_ID(), 'category', array(
                                'fields' => 'names'
                            ));
                            if ($terms) {
        //                        $html .= implode(', ', $terms);
                                $html .= $terms[0];
                            }
                    $html .= '   
                    </span>
                </div>
                <div class="card-block news__item_block">
                    <p class="card-text news__item_text">'.  get_the_excerpt($post_id) .'</p>
                    <a href="'. get_the_permalink($post_id) .'" class="news__item_link">'.__("Читать далее", THEME_OPT).'</a>
                </div>
            </div>
        </article>';

    return $html;
}






/**
 * Custom WP Routing rules
 */
require get_parent_theme_file_path( '/inc/route.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );




