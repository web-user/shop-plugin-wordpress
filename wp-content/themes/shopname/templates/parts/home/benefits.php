<?php
/**
 * Template part for displaying a benefits block
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */
?>

<section class="section block__advantages advantages bg-grey" id="blockAdvantages">
    <block>

    <?php
    $args = array(
        'posts_per_page' => -1,
        'post_type'      => 'benefits',
        'post_status'    => 'publish',
    );

    $the_query = new WP_Query($args); ?>

    <?php if ($the_query->have_posts()) : ?>

            <div class="container">
                <h2 class="section__title mt-0"><?php _e('Преимущества', THEME_OPT); ?></h2>
            </div>

            <div class="advantages__pagination_wrapper">
                <div class="container">
                    <div id="advantagesPagination" class="owl-carousel advantages__pagination">
                        <?php
                        $key = 1;

                        while ($the_query->have_posts()) : $the_query->the_post();
                            $adv_icon = get_field('adv_icon');
                            $default_icon = get_stylesheet_directory_uri() . '/assets/images/advantages/icons/infrastructure.png';

                            $icon_url = ( ! empty($adv_icon)) ? $adv_icon['url'] : $default_icon;
                            ?>
                            <div class="item">
                                <a role="button"
                                   class="advantages__btn <?php echo ($key == 1) ? 'active' : ''; ?> id-<?php echo $post->ID; ?>"
                                   href="#advantages_<?php echo $key; ?>"
                                   data-id="<?php echo $post->ID; ?>"
                                >
                                    <span class="advantages__btn_icon" style="background-image: url(<?php echo $icon_url; ?>)"></span>
                                <span class="advantages__btn_title">
                                    <?php the_title(); ?>
                                </span>
                                </a>
                            </div>
                            <?php
                            $key++;
                        endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                    </div>
                    <div id='advantages-custom-dots' class='owl-dots'>
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <div class='owl-dot id-<?php echo $post->ID; ?>' data-id="<?php echo $post->ID; ?>"><span></span></div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
            <div id="advantagesCarousel" class="owl-carousel advantages__carousel">
                <?php
                $key = 1;

                while ($the_query->have_posts()) : $the_query->the_post();
                    $adv_bg = get_field('adv_bg');
                    $default_bg = get_stylesheet_directory_uri() . '/assets/images/advantages/bg/infrastructure.png';

                    $bg_url = ( ! empty($adv_bg)) ? $adv_bg['sizes']['shopname-featured-image'] : $default_bg;
                    ?>
                    <div class="item" data-hash="advantages_<?php echo $key; ?>">
                        <div class="section_container height-auto advantages__carousel_bg" style="background-image: url(<?php echo $bg_url; ?>)">

                                <div class="advantages__carousel_bg-blur-wrapper"></div>
                                <div class="container">
                                    <div class="advantages__carousel_info">
                                        <div class="advantages__carousel_title">
                                            <?php the_title(); ?>
                                        </div>
                                        <div class="advantages__carousel_content">
                                            <?php

                                            $content = mb_substr(get_the_content(), 0, 950);
                                            echo apply_filters( 'the_content', $content);

                                            ?>
                                        </div>
                                    </div>
                                    
                                </div>

                        </div>
                    </div>

                    <?php
                    $key++;
                endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div><!-- #advantagesCarousel -->

    <?php endif; ?>

    </block>
</section><!-- #advantages -->
