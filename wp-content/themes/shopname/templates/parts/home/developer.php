<?php
/**
 * Template part for displaying a developer brands
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

if (CURRENT_LANG == 'ua') {
    $tax_query = array(
        array(
            'taxonomy' => 'brand_category',
            'field'    => 'slug',
            'terms'    => 'developer-ua'
        ),
    );
} else {
    $tax_query = array(
        array(
            'taxonomy' => 'brand_category',
            'field'    => 'slug',
            'terms'    => 'developer'
        ),
    );
}

$args = array(
    'posts_per_page' => 1,
    'post_type'      => 'brand',
    'post_status'    => 'publish',
    'tax_query'      => $tax_query,
);

$the_query = new WP_Query($args); ?>

<?php if ($the_query->have_posts()) : ?>
    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <div class="block__about_text text-justify">

            <?php the_field('about_dev'); ?>

            <a href="<?php the_permalink(); ?>"><?php _e('Читать далее', THEME_OPT); ?></a>
        </div>
        <?php if( have_rows('developer_block') ): ?>
            <div class="row no-gutters">
            <?php while ( have_rows('developer_block') ) : the_row(); ?>
                <div class="col-lg-4">
                    <a href="<?php the_permalink(); ?>" class="block__about_info">
                        <?php
                        $dev_img = get_sub_field('dev_img');

                        if ( !empty($dev_img) ): ?>
                            <span class="block__about_info-img" style="background-image: url(<?php echo $dev_img['sizes']['large']; ?>)"></span>
                        <?php else: ?>
                            <span class="block__about_info-img"></span>
                        <?php endif; ?>

                        <span class="block__about_info-btn">
                            <?php the_sub_field('dev_title'); ?>
                        </span>
                        <span class="block__about_info-text">
                            <?php the_sub_field('dev_desc'); ?>
                        </span>
                    </a>
                </div>
            <?php endwhile; ?>
            </div>
        <?php endif; ?>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
