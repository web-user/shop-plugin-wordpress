<?php
/**
 * Template part for displaying a complex brands
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

if (CURRENT_LANG == 'ua') {
    $tax_query = array(
        array(
            'taxonomy' => 'brand_category',
            'field'    => 'slug',
            'terms'    => 'complex-ua'
        ),
    );
} else {
    $tax_query = array(
        array(
            'taxonomy' => 'brand_category',
            'field'    => 'slug',
            'terms'    => 'complex'
        ),
    );
}

$args = array(
    'posts_per_page' => 1,
    'post_type'      => 'brand',
    'post_status'    => 'publish',
    'tax_query'      => $tax_query
);

$query = new WP_Query($args); ?>
<?php if ($query->have_posts()) : ?>
    <?php while ($query->have_posts()) : $query->the_post(); ?>

        <h3 data-animate="fromLeft" animate-speed=1000><?php the_field('complex_title'); ?></h3>

        <div data-animate="fromRight" animate-speed=3000><?php the_field('complex_desc'); ?></div>

        <a href="<?php the_permalink(); ?>" data-animate="fromRight" animate-speed=5000><?php _e('Читать далее', THEME_OPT); ?></a>

    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
