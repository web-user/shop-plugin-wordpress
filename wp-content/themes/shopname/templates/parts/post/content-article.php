<?php
/**
 * Template part for displaying single post as article
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

?>
<div class="col-md-4 col-sm-6">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="card news__item">
            <?php if (has_post_thumbnail()) : ?>
                <div class="news__item_img" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>)" alt="Card image cap">
                    <div class="news__item_date">
                        <div class="day"><?php echo get_the_date('d'); ?></div>
                        <div class="month"><?php echo get_the_date('M'); ?></div>
                        <div class="year"><?php echo get_the_date('Y'); ?></div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="card-block news__item_header">
                <h2 class="card-title news__item_title">
                    <?php
                    $strings_actual_encoding = 'UTF-8';
                    $s_noentities = html_entity_decode(get_the_title(), ENT_QUOTES, $strings_actual_encoding);
                    echo mb_substr($s_noentities, 0, 36, $strings_actual_encoding);
                    echo (mb_substr(get_the_title(), 37)) ? '...' : '';
                    ?>
                </h2>
                <span class="news__item_tag">
                    <?php
                    $terms = wp_get_post_terms(get_the_ID(), 'category', array(
                        'fields' => 'names'
                    ));
                    if ($terms) {
//                        echo implode(', ', $terms);
                        echo $terms[0];
                    }
                    ?>
                </span>
            </div>
            <div class="card-block news__item_block">
                <p class="card-text news__item_text">
                    <?php
                    $excerpt = get_the_content();
                    $excerpt = preg_replace(" ([.*?])",'',$excerpt);
                    $excerpt = strip_shortcodes($excerpt);
                    $excerpt = strip_tags($excerpt);
                    $excerpt = substr($excerpt, 0, 100);
                    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
                    $excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));

                    echo $excerpt . '...';
                    ?>
                </p>
                <a href="<?php echo get_the_permalink() ?>" class="news__item_link"><?php _e('Читать далее', THEME_OPT); ?></a>
            </div>
        </div>
    </article>
</div>