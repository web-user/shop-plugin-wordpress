<?php
/**
 * Template part for displaying a posts
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
$current_cat = get_query_var('category_name') ? get_query_var('category_name') : 'city';

$args = array(
    'posts_per_page' => 9,
    'post_type'      => 'post',
    'paged'          => $paged,
    'post_status'    => 'publish',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => $current_cat,
        ),
    ),
);



$the_query = new WP_Query($args); ?>

<div class="col-lg-9">
    <block>
        <div class="news__items_wrapper">
            <div class="row">

            <?php if (have_posts()) :
                while (have_posts()) : the_post(); ?>

                    <?php get_template_part( 'templates/parts/post/content', 'article' ); ?>

                <?php
                endwhile;
                wp_reset_postdata(); ?>
            </div><!-- .row -->
            <nav>
                <!-- Pagination -->
                <?php $pagenavi = get_pagenavi_array($wp_query); ?>
                <?php if (is_array($pagenavi)) : ?>
                    <ul class="pagination justify-content-center news__items_pagination">
                        <?php foreach ($pagenavi as $link) : ?>
                            <?php
                            preg_match('/<span class="(.*?) dots">(.*?)<\/span>/uis', $link, $matches);
                            if (count($matches) > 0) {
                                $link = '<div class="page-gap"><div></div><div></div><div></div></div>';
                            }
                            ?>
                            <li class="page-item"><?php echo $link; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <!-- /Pagination -->
            </nav>

            <?php endif; ?>
        </div>
    </block>
</div>
