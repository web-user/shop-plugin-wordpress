<?php

/**
 * Template part for displaying a related posts
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */
$taxonomy = 'category';
$terms = wp_get_post_terms($post->ID, $taxonomy, array('fields' => 'slugs'));

/* Get Current post */
$current_post = $post->ID;

if ($terms) :

    $postlist = get_posts( array(
        'posts_per_page' => -1,
        'post_type'      => 'post',
        'post_status'    => 'publish',
        'tax_query'      => array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => $terms,
            ),
        ),
    ));

    if ($postlist) : ?>
        <div class="col-md-12 col-lg-9 carousel__related_wrapper">
            <div id="related-news" class="owl-carousel owl-theme carousel__related">
                <?php
                $arrayKeys = array_keys($postlist);
                $lastArrayKey = array_pop($arrayKeys);

                $posts = array();
                foreach ( $postlist as $key => $post ) { setup_postdata($post);

                    /* Get array posts ID */
                    $posts[] += $post->ID;

                    if ($current_post == $post->ID) {
                        if (count($posts) === 1) { // If the first post
                            $pos_number = count($posts) + 1;
                        } elseif ($lastArrayKey == $key) { // If the last post
                            $pos_number = count($posts) - 1;
                        } else {
                            $pos_number = count($posts);
                        }
                        $current_classes = 'current-news news-position-'.$pos_number;
                    } else {
                        $current_classes = '';
                    }
                    ?>
                        <article id="post-<?php echo $post->ID ?>" class="<?php echo implode(' ', get_post_class($current_classes)) ?>">
                            <div class="card news__item">
                                <?php
                                if (has_post_thumbnail()) : ?>
                                    <div class="news__item_img" style="background-image: url(<?php the_post_thumbnail_url("full"); ?>)" alt="Card image cap">
                                        <div class="news__item_date">
                                            <div class="day"><?php echo get_the_date("d", $post->ID); ?></div>
                                            <div class="month"><?php echo get_the_date("M", $post->ID); ?></div>
                                            <div class="year"><?php echo get_the_date("Y", $post->ID); ?></div>
                                        </div>
                                    </div>
                                    <?php
                                endif; ?>
                                <div class="card-block news__item_header">
                                    <h2 class="card-title news__item_title">
                                        <?php
                                        $strings_actual_encoding = 'UTF-8';
                                        $s_noentities = html_entity_decode(get_the_title(), ENT_QUOTES, $strings_actual_encoding);
                                        echo mb_substr($s_noentities, 0, 36, $strings_actual_encoding);
                                        echo (mb_substr(get_the_title(), 37)) ? '...' : '';
                                        ?>
                                    </h2>
                                    <span class="news__item_tag">
                                        <?php
                                            $terms = wp_get_post_terms($post->ID, 'category', array(
                                                'fields' => 'names'
                                            ));
                                            if ($terms) {
                        //                        echo implode(', ', $terms);
                                                echo $terms[0];
                                            }
                                            ?>
                                    </span>
                                </div>
                                <div class="card-block news__item_block">
                                    <p class="card-text news__item_text">
                                        <?php
                                        $excerpt = get_the_content($post->ID);
                                        $excerpt = preg_replace(" ([.*?])",'',$excerpt);
                                        $excerpt = strip_shortcodes($excerpt);
                                        $excerpt = strip_tags($excerpt);
                                        $excerpt = substr($excerpt, 0, 100);
                                        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
                                        $excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));

                                        echo $excerpt . '...';
                                        ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="news__item_link"><?php _e("Читать далее", THEME_OPT); ?></a>
                                </div>
                            </div>
                        </article>

                    <?php
                }
                wp_reset_postdata(); ?>
            </div><!-- #related-news -->
        </div>

        <?php
    endif; // if ($postlist)
endif; // if ($terms)
