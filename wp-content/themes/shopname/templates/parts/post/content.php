<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

$compare_date = '2017-07-15';
$thumb_class = ($compare_date > get_the_date('Y-m-d')) ? 'article__small_img' : '' ;
?>
<?php if ( wp_get_referer() ) : ?>
    <a class="link__return" href="<?php echo wp_get_referer() ?>">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <?php _e('Назад', THEME_OPT); ?>
    </a>
<?php else : ?>
    <a class="link__return" href="<?php echo get_home_url() ?>">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <?php _e('Назад', THEME_OPT); ?>
    </a>
<?php endif; ?>



<article id="post-<?php the_ID(); ?>" <?php post_class($thumb_class); ?>>
    <div class="article__wrapper">
        <div class="article__title">
            <div class="news__item_date">
                <div class="day"><?php echo get_the_date('d'); ?></div>
                <div class="month"><?php echo get_the_date('M'); ?></div>
                <div class="year"><?php echo get_the_date('Y'); ?></div>
            </div>
            <?php  the_title( '<h1>', '</h1>' ); ?>

            <div class="article__tag">
                <?php
                $terms = wp_get_post_terms(get_the_ID(), 'category', array(
                    'fields' => 'names'
                ));
                if ($terms) :
//                    echo implode(', ', $terms);
                    echo $terms[0];
                endif;
                ?>
            </div>
        </div>
        <div class="article__img" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>)"></div>
        <div class="article__text_wrapper">
            <div class="row justify-content-md-center">
                <div class="col-lg-9">
                    <div class="article__text text-justify">

                        <?php
                        the_content();

                        if (function_exists('have_rows')) :
                            if( have_rows('socials') ): ?>
                                <div class="social">

                                    <?php while ( have_rows('socials') ) : the_row(); ?>

                                        <a href="<?php the_sub_field('link') ?>" target="_blank" class="icon icon-<?php the_sub_field('social') ?>"></a>

                                    <?php endwhile; ?>
                                </div><!-- .social -->
                            <?php endif;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="article__line"></div>

    <!-- Related Posts -->
    <div class="news__items_wrapper">
        <div class="row justify-content-md-center">

            <?php get_template_part('templates/parts/post/related') ?>

        </div>
    </div>

</article><!-- #post-## -->
