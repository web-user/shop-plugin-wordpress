<?php

/**
 * Template Name: News
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

$current_lang = get_locale();

//if ($current_lang == 'ua') {
//    $current_cat = get_query_var('current_cat') ? get_query_var('current_cat') : 'novosti-kompleksa-ua';
//} else {
//    $current_cat = get_query_var('current_cat') ? get_query_var('current_cat') : 'city';
//}


get_header(); ?>

<section class="news">

    <?php the_title('<h1 class="sr-only">', '</h1>'); ?>

    <div class="container">
        <div class="row">

            <?php while ( have_posts() ) : the_post(); ?>

                <!-- Left Sidebar -->
                <div class="col-lg-3">
                    <div class="buttons__navigation">
                        <?php
                        $args = array(
                            'type'         => 'post',
                            'taxonomy'     => 'category',
                        );

                        $categories = get_categories( $args );
                        if ($categories) :


                            foreach ($categories as $cat) : ?>

                                <a href="<?php echo ( $current_lang == 'ua' ) ? home_url('ua/news') .'/'.$cat->slug : home_url('news') .'/'.$cat->slug; ?>" class="buttons__navigation_item"><?php echo $cat->name; ?></a>
                            <?php endforeach; ?>

                            <button type="button" class="buttons__navigation_item-toggle dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu buttons__navigation_dropdown">
                                <?php foreach ($categories as $cat) : ?>

                                    <a href="<?php echo ( $current_lang == 'ua' ) ? home_url('ua/news') .'/'.$cat->slug : home_url('news') .'/'.$cat->slug; ?>" class="buttons__navigation_item"><?php echo $cat->name; ?></a>

                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="col-lg-9">
                    <block>
                        <div class="news__items_wrapper">
                            <div class="row">

                            <?php
                            $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

                            $args = array(
                                'posts_per_page' => 9,
                                'post_type'      => 'post',
                                'paged'          => $paged,
                                'post_status'    => 'publish'
                            );

                            $the_query = new WP_Query($args); ?>

                            <?php if ($the_query->have_posts()) :
                                while ($the_query->have_posts()) : $the_query->the_post(); ?>

                                    <?php get_template_part( 'templates/parts/post/content', 'article' ); ?>

                                    <?php
                                endwhile;
                                wp_reset_postdata(); ?>
                            </div><!-- .row -->
                            <nav>
                                <!-- Pagination -->
                                <?php $pagenavi = get_pagenavi_array($the_query); ?>
                                <?php if (is_array($pagenavi)) : ?>
                                    <ul class="pagination justify-content-center news__items_pagination">
                                        <?php foreach ($pagenavi as $link) : ?>
                                            <?php
                                            preg_match('/<span class="(.*?) dots">(.*?)<\/span>/uis', $link, $matches);
                                            if (count($matches) > 0) {
                                                $link = '<div class="page-gap"><div></div><div></div><div></div></div>';
                                            }
                                            ?>
                                            <li class="page-item"><?php echo $link; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                <!-- /Pagination -->
                            </nav>

                            <?php endif; ?>
                        </div>
                    </block>
                </div>

            <?php endwhile; ?>

        </div><!-- row -->
    </div><!-- container -->
</section>

<?php get_template_part('templates/parts/page/content', 'footer'); ?>

<?php get_footer(); ?>
