<?php

/**
 * Template Name: Completion of apartments
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

get_header();

?>


<!-- Start section complectation -->
<?php if( have_posts() ): ?>
    <section id="complectation" class="section__js complectation">
        <?php the_post(); ?>
        <?php get_template_part( 'templates/parts/apartment/complectation' ); ?>
    </section>
<?php endif; ?>
<!-- End section complectation -->

<?php get_footer(); ?>