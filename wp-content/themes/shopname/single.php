<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage shopname
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <section class="article section__main">
        <div class="container">
            <block>

                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post();

                        get_template_part( 'templates/parts/post/content' );

                    endwhile; // End of the loop.
                    ?>

            </block>
        </div><!-- .container -->

    </section>

<?php get_template_part('templates/parts/page/content', 'footer'); ?>

<?php get_footer(); ?>