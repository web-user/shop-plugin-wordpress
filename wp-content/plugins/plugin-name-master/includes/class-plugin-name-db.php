<?php
/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */

namespace oop\includes;



class DB_conect {

	/**
	 * DB_conect constructor.
	 */
	protected function __construct() {
		global $wpdb;

		$this->db = $wpdb;
		$this->table = $this->db->prefix . 'newsletter_date';
	}

	public function get_all_results(){

		return $this->db->get_results( "SELECT * FROM $this->table " );

	}

	public function delete_user_data($data_user){
		$data = $data_user;

		$ids = array_map(function($item) {
			return $item['id'];
		}, $data);

		$ids = implode(',', $ids);
		$where = "id IN($ids)";


		$this->db->query("DELETE FROM $this->table WHERE $where" );
	}

	public function edit_data_user_db($data_tabledata_field, $data_where) {
		return $this->db->update(
			$this->table,
			$data_tabledata_field,
			$data_where

		);
	}
}