<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */

namespace name\includes;


use oop\includes\DB_conect;

class Plugin_Name_Ajax extends DB_conect {


	public function __construct() {

		parent::__construct();

	}


	protected function delete_data_user() {

		$data = $_POST['data'];

		$this->delete_user_data($data);

		exit();
	}

	public function get_current_user_info(){

        // проверяем nonce код, если проверка не пройдена прерываем обработку
        check_ajax_referer( 'myajax-nonce', 'nonce_code' );
        // или так
        if( ! wp_verify_nonce( $_POST['nonce_code'], 'myajax-nonce' ) ) die( 'Stop!');

        // обрабатываем данные и возвращаем
        echo 'Возвращаемые данные';

        // не забываем завершать PHP
        wp_die();
	}

	public function serialize_ajax_data($post_data){
		$params_array = array();

		parse_str($post_data, $params_array);

		return $params_array;
	}

	public function edit_data_user(){

		$params = $this->serialize_ajax_data( $_POST['data'] );

//		print_r($params);

		$data_tabledata_field = array( 'email' => $params['email'], 'status' => $params['status'], );

		$data_where =  array( 'id' => $params['id'] );


		$this->edit_data_user_db($data_tabledata_field, $data_where);



		exit();

	}
}