<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class Plugin_Name_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		/* Test data
		INSERT INTO `wp_cca_newsletter` (`id`, `email`, `first_name`, `status`, `rating`, `register_date`, `last_changed`) VALUES ('4', 'wapuu@wordpress.example1www', 'Vaasa', '8', '2', '2017-08-07', '2017-08-07')
		*/ 

		global $wpdb;
		/* require db Delta */
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		$sql = "CREATE TABLE IF NOT EXISTS `" . $wpdb->base_prefix . "newsletter_date` (
			  `id` int(11) UNSIGNED NOT NULL,
			  `email` varchar(255) NOT NULL,
			  `first_name` varchar(255) NOT NULL,
			  `status` varchar(255) NOT NULL,
			  `rating` varchar(255) NOT NULL,
			  `register_date` DATE ,
			  `last_changed` DATE, 
			UNIQUE KEY (id)
			);";
		/* call dbDelta */
		dbDelta( $sql );
	}

}
