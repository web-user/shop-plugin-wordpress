<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */

namespace name\includes;

class Plugin_Name_Menu {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_menu() {

		add_menu_page(
			'Sitepoint WP_OOP Example',
			'SP WP_OOP Example',
			'manage_options',
			'wp_oop_sett_class',
			[ $this, 'plugin_settings_page_oop' ]
		);

	}

	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page_oop() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/plugin-name-admin-display.php';

    }



}
