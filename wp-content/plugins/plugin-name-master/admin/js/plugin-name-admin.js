(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	console.log('admin test script');

    $( document ).ready(function() {
        $( "#ajax-head-name" ).on( "click", function(e) {
            e.preventDefault();


            var data = 'Res other';
            $.ajax({
                url: ajaxurl,
				type: 'post',
                data: {
                    action: 'get_current_user_info',
					my_data: data
                },
                // beforeSend : function( d ) {
                //     console.log( 'Before send', d );
                // },
                success: function(res){
                    console.log(res);
                },
				error: function () {
					console.log('Error');
                }
            });

            console.log('Click now');
        });
	});





    $( document ).ready(function() {

        function sendDeleteData(data) {
            return jQuery.ajax({
                url: ajaxurl,
                type: "POST",
                data: {
                    action: 'delete_data_info',
                    data: data
                }
            });
        }




        var self = this;

        self.tableHTML = jQuery('#example');

        self.tableData = self.tableHTML.DataTable(getDataTableConfig());

        self.toggleAllCheckboxes = jQuery('#bulkDelete');

        self.form = jQuery('form[name="dataForm"]');
        self.actionInput = self.form.find('[name="action"]');

        self.addSubscriber = jQuery('#addSubscriber');

        function getDataTableConfig() {
            return {
            "processing": true,
            ajax: {
                url: ajaxurl,
                type: 'POST',
                data: function(data) {
                    data.action = 'get_current_user_info'
                }
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    text: 'Bulk Trash',
                    className: 'button',
                    init: function ( dt, $button, config ) {
                        $button.click( function () {
                            var $rows = self.tableHTML.find('tbody td:first-child input[type="checkbox"]:checked').closest('tr');
                            if ($rows.length === 0)
                                return;

                            if (!confirm('Are you sure you want to delete all selected subscribers?'))
                                return;

                            var rows = self.tableData.rows($rows);
                            var user_data = [];
                            rows.data().each(function (rowData) {
                                user_data.push({
                                    id: rowData.id
                                });
                            });

                            console.log(user_data);

                            sendDeleteData(user_data).success(function (data) {
                                console.log(data);
                                rows.remove().draw(false);
                                self.toggleAllCheckboxes.removeAttr('checked');
                            });
                        });
                    }

                }
            ],
            "aoColumns": [
                {
                    mData: 'id',
                    "bSortable": false,
                    "mRender": function ( url, type, full )  { return   '<input type="checkbox"  class="deleteRow" value="'+url+'">'; }

                },
                {
                	mData: 'email',
					"bSortable": false,
                    "mRender": function ( data, full ) {
                        return '<span>' + data.email + '</span><div class="row-actions"><a href="#" class="editSubscriber" data-id="' +
                            data.id + '">Edit</a>|<a href="#" class="delete deleteSubscriber" data-id="' +
                            data.id + '">Trash</a></div>';
                    }

				},
                {
                	mData: 'first_name',
                 	"mRender": function ( url, type, full )  { return  '<a href="'+url+'">' + url + '</a>'; }
				},
                {
                	mData: 'status'
					// "visible": false
                },
                {
                    mData: 'rating',
                    // "visible": false
                    "mRender": function ( data, type, full, meta ) {
                        var stars = new Array(+data + 1).join('&#x2605');
                        return '<span class="stars" data-number="' + data + '">' + stars + '</span>';
                    }
                }
            ] };
        }

        function setAction(action) {
            self.actionInput.val(action);
        }

        function hideForm(hide) {
            if(hide)
                self.form.slideUp();
            else
                self.form.slideDown();
        }

        function fetchDataRowToForm(row) {
            var rowData = row.data();
            var fields = ['id', 1, 2, 3, 4];

            // console.log(fields)
            console.log(rowData)

            $('#user-mail').attr('value', rowData.email.email);
            $('#user-id-hidden').attr('value', rowData.email.id);

            console.log($('#user-mail'))





        }


        self.tableHTML.on('click', '.deleteSubscriber', function(e) {
            e.preventDefault();
            hideForm(true);
            var $row = jQuery(this).closest('tr');
            $row.addClass('selected');
            var row = self.tableData.row($row);
            var data = row.data();
            sendDeleteData([{
                id: data.id
            }]).done(function() {
                row.remove().draw(false);
            });
        });

        self.tableHTML.on('click', '.editSubscriber', function(e) {
            e.preventDefault();
            var row = jQuery(this).closest('tr');
            self.tableHTML.find('tr.selected').removeClass('selected');
            row.addClass('selected');

            jQuery('html, body').animate({scrollTop: 0}, 200);
            hideForm(false);
            setAction('cca_edit_subscriber');
            fetchDataRowToForm(self.tableData.row(row));
        });


        self.form.submit( function(e) {
            e.preventDefault();
            var formCtx = this;

            jQuery.ajax({
                url: ajaxurl,
                type: "POST",
                data: {
                    data:jQuery(formCtx).serialize(),
                    action:'edit_data_info'
                },
                success: function (res) {
                    console.log(res);
                    self.tableData.ajax.reload(null, false);
                    formCtx.reset();
                    hideForm(true);
                    self.tableHTML.find('tr.selected').removeClass('selected');
                }
            });
        });






        $("#bulkDelete").on('click',function() { // bulk checked
            var status = this.checked;
            $(".deleteRow").each( function() {
                $(this).prop("checked",status);
            });
        });

        // new $.fn.dataTable.Buttons( table, {
        //     buttons: [
        //         'copy', 'excel', 'pdf'
        //     ]
        // } );





        var oTable =  $('#example'). dataTable();
        $('#example').on('click', 'tr', function(){
            var oData = oTable.fnGetData(this);
            console.log(oData);
        })


    });

})( jQuery );
