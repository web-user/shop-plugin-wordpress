<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
    <h2>WP OOP Class Example34</h2>


    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
            <div id="post-body-content">
                <div class="meta-box-sortables ui-sortable">
                    <h2><a href="#" id="ajax-head-name">Click here!</a></h2>


                    <div class="subscriber-form table-wrapper">

                        <form name="dataForm">
                            <input type="hidden" name="id" value="" id="user-id-hidden">

                            <table class="display dataTable">
                                <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>First Name</th>
                                    <th>Status</th>
                                    <th>Rating</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <input type="email" name="email" placeholder="Subscriber email" id="user-mail" value="" required maxlength="100">
                                    </td>
                                    <td style="font-weight: normal;">
                                        <input type="text" name="first_name" placeholder="Subscriber first name" maxlength="60">
                                    </td>
                                    <td>
                                        <select name="status">
                                            <option value="pending">Pending</option>
                                            <option value="email_sequence">Email sequence</option>
                                            <option value="active">Active</option>
                                        </select>
                                    </td>
                                    <td><input type="number" name="rating" min="1" max="5" value="1"></td>
                                    <td></td>
                                    <td><input type="submit" value="Save" class="button"></td>
                                </tr>
                                </tbody>
                            </table>

                            <input type="hidden" name="action">
                            <input type="hidden" name="prev_status">
                        </form>


                    </div>


                    <div class="">
                        <table id="example" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th><input type="checkbox"  id="bulkDelete"  /></th>
                                <th>Email</th>
                                <th>First Name</th>
                                <th>Status</th>
                                <th>Rating</th>

                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br class="clear">
    </div>
</div>