(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	console.log('FRONT script')




    $( document ).ready(function() {
        $( "#ajax-head-name" ).on( "click", function(e) {
            e.preventDefault();


            var data = 'Res other';
            $.ajax({
                url: myajax.url,
                type: 'post',
                data: {
                    action: 'get_current_user_info',
                    my_data: data,
                    nonce_code : myajax.nonce
                },
                // beforeSend : function( d ) {
                //     console.log( 'Before send', d );
                // },
                success: function(res){
                    console.log(res);
                },
                error: function () {
                    console.log('Error');
                }
            });

            console.log('Click now');
        });
    });

})( jQuery );
